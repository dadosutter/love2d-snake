-------------------------------------------------------------------------------
--
-- Love2D Snake
--
-- Dado Sutter
-- Sep 2018
--
-- ## ToDo:
--    Pensar se vale a pena tratar wrap arround
--    Tratar melhor o fim do jogo
--    Introduzir random goodies, com pontos diferentes
--    Introduzir score
--    Muito tempo sem pegar goodies perde ponto?
--    Acelerar depois de algum tempo ou evento?
--
--
-------------------------------------------------------------------------------

-- Configurable parameters
bodySize = 10           -- size of the initial body square side
timeToUpdate = 0.1      -- game tick in seconds



function love.load()
  -- Constant vectors to produce directions
  turnE     = {x = 1,   y = 0}
  turnW     = {x = -1,  y = 0}
  turnN     = {x = 0,   y = -1}
  turnS     = {x = 0,   y = 1}
  turnNE    = {x = 1,   y = -1}
  turnSE    = {x = 1,   y = 1}
  turnSW    = {x = -1,  y = 1}
  turnNW    = {x = -1,  y = -1}

  alive = true
  mustGrow = false
  lastUpdate = 0

  deathSound = love.audio.newSource('death.mp3', 'static')

  bg = love.graphics.newImage('bg.jpg')
  --## There must be a function that returns W & H on a single call. getMode()??
  bgW = bg:getWidth()
  bgH = bg:getHeight()
  -- Window size is half because bg will be scaled down to half too
  love.window.setMode(bgW/2, bgH/2)   
  love.graphics.setColor(55, 80, 160)
  love.window.setTitle('Snake Catolica Jaragua do Sul, SC')
  canvasW, canvasH = love.window.getMode()

  -- Start with snake head in the middle and moving East/Right
  step = turnE
  x, y = (canvasW-bodySize)/2, (canvasH-bodySize)/2

  -- Create snake body
  body = {}
  for i=0, 8 do
    table.insert(body, {x=x-(bodySize)*i, y=y})
  end

  -- Some goodies to feed the snake  
  goodie = 
  {
    x = x + 10*bodySize, 
    y = y,
    points = 2
  }
  math.randomseed(os.time())
  goodie =
  {
    x = math.random(0, canvasW-bodySize),
    y = math.random(0, canvasH-bodySize),
    points = math.random(1, 5)
  }
  

end





function hitBorder()
  if x>0 and x<(canvasW-bodySize) and y>0 and y<(canvasH-bodySize) then
    return false
  else
    return true
  end
end





function hitItself()
  for _,pos in pairs(body) do
    if pos.x == x and pos.y == y then
      return true
    end
  end
  return false
end





function hitGoodie()
  if x >= goodie.x and x <= goodie.x+bodySize and 
  y >= goodie.y and y <= goodie.y+bodySize then
    return true
  else
    return false
  end
end





function love.keypressed(key, scancode, isrepeat)
  if love.keyboard.isDown('kp1') then 
    step = turnSW
  elseif love.keyboard.isDown('kp2') then
    step = turnS
  elseif love.keyboard.isDown('kp3') then
    step = turnSE
  elseif love.keyboard.isDown('kp4') then
    step = turnW
  elseif love.keyboard.isDown('kp6') then
    step = turnE
  elseif love.keyboard.isDown('kp7') then
    step = turnNW
  elseif love.keyboard.isDown('kp8') then
    step = turnN
  elseif love.keyboard.isDown('kp9') then
    step = turnNE
  elseif love.keyboard.isDown('space') then
    love.load()
  elseif love.keyboard.isDown('g') then
    mustGrow = true
  end
end





function love.update(dt)
  lastUpdate = lastUpdate + dt
  if (lastUpdate >= timeToUpdate) and alive then
    x = x + (step.x * bodySize)
    y = y + (step.y * bodySize)

    if hitGoodie() then
      mustGrow = true
      goodie =
      {
        x = math.random(0, canvasW-bodySize),
        y = math.random(0, canvasH-bodySize),
        points = math.random(1, 5)
      }
      
    end

    if hitItself() or hitBorder() then 
      alive = false
      deathSound:play()
--        love.audio.play(deathSound)
    else
      table.insert(body, 1, {x=x, y=y})  -- grow head to "move"
      if mustGrow then                   -- don't remove tail if must grow
        mustGrow = false
      else
        table.remove(body, #body)        -- remove tail to "move"
      end
    end
    lastUpdate = 0
  end
end





  function love.draw()
    love.graphics.draw(bg,0,0,0,0.5,0.5)              -- background
    r, g, b = love.graphics.getColor()        -- save rgb to restore
    love.graphics.setColor(0, 0, 255)
    love.graphics.print(goodie.points, goodie.x, goodie.y)
    love.graphics.rectangle('line', goodie.x, goodie.y, bodySize, bodySize)
    love.graphics.setColor(r, g, b)
    for i, pos in pairs(body) do
      love.graphics.rectangle('fill', pos.x, pos.y, bodySize, bodySize)
    end

    if not alive then
      love.graphics.setColor(255, 0, 0)
    end
  end